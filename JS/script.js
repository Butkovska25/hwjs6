/*
1. Пошук конкретних символів за допомогою \
2. Function Announce (){};
3. Hoisting переміщує оголошення функцій, змінних або класів на початок їхньої області видимості перед виконанням коду (нагору).
*/

function createNewUser() {
  newUser = {
    firstName: prompt('Please, enter your first name'),
    lastName: prompt('Please, enter your last name'),
    birthday: prompt('Please, enter your birthday (dd.mm.yyyy)'),
    get getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase()
    },
    set setfirstName(newFirstName) { this.firstName = newFirstName },
    get getAge() {
      return new Date().getFullYear() - new Date(this.birthday.substring(6)).getFullYear()
    },
    get getPassword() {
      return (this.firstName[0]).toUpperCase() + (this.lastName).toLowerCase() + this.birthday.substring(6)
    }}};

createNewUser();

console.log(newUser.getLogin);
console.log(newUser.getAge);
console.log(newUser.getPassword);
